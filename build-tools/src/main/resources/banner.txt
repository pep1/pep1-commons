
                      _
                    /' )
 _ _      __   _ _ (_, |
( '_`\  /'__`\( '_`\ | |
| (_) )(  ___/| (_) )| |
| ,__/'`\____)| ,__/'(_)
| |           | |
(_)           (_)

:: Application Version: ${application.version}
:: Spring Boot Version: ${spring-boot.version}