/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.jpa.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.h2.H2ConsoleAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pep1.commons.hashid.HashIdService;
import pep1.commons.jpa.error.JpaHandlerExceptionAdvice;
import pep1.commons.jpa.jackson.PageDeserializer;
import pep1.commons.jpa.jackson.PageSerializer;
import pep1.commons.util.AutowireHelper;

@Slf4j
@Configuration
@EnableTransactionManagement
@Import({
        DataSourceAutoConfiguration.class,
        LiquibaseAutoConfiguration.class,
        H2ConsoleAutoConfiguration.class
})
public class JpaConfig {

    @Bean
    public AutowireHelper autowireHelper() {
        return new AutowireHelper();
    }

    @Bean
    @ConditionalOnMissingBean(HashIdService.class)
    public HashIdService hashId() {
        return new HashIdService("verycomplicatedsalt", 6);
    }

    @Bean
    public JpaHandlerExceptionAdvice jpaHandlerExceptionAdvice() {
        return new JpaHandlerExceptionAdvice();
    }

    @Bean
    public Module pageModule() {
        return new SimpleModule()
            .addDeserializer(org.springframework.data.domain.Page.class, new PageDeserializer())
            .addSerializer(org.springframework.data.domain.Page.class, new PageSerializer());
    }

}
