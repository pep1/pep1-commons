/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.jpa.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import pep1.commons.domain.dto.MetaDataDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 9/2/15.
 */
@Slf4j
public class PageDeserializer extends JsonDeserializer<Page> {

    @Override
    public Page deserialize(final JsonParser jsonParser, final DeserializationContext ctxt) throws IOException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);

        JsonNode metaDataNode = node.get("meta");
        MetaDataDTO metaDataDTO;
        Class<?> contentClass = Object.class;
        PageRequest request = null;
        Long totalItems = null;

        if (null != metaDataNode) {
            metaDataDTO = oc.readValue(metaDataNode.traverse(), MetaDataDTO.class);
            if (null != metaDataDTO) {
                contentClass = guessClassByName(metaDataDTO.getContentType());
                request = new PageRequest(metaDataDTO.getPage(), metaDataDTO.getPerPage());
                totalItems = metaDataDTO.getTotalItems();
            }
        }

        JsonNode contentNode = node.get("content");
        List<Object> contentObjects = new ArrayList<>();

        if (null != contentNode && contentNode.isArray()) {
            for (final JsonNode objNode : contentNode) {
                Object object = oc.readValue(objNode.traverse(), contentClass);

                if (log.isDebugEnabled()) {
                    log.debug(object.toString());
                }

                contentObjects.add(object);
            }
        }

        return new PageImpl<>(contentObjects, request, (null != totalItems) ? totalItems : contentObjects.size());
    }

    private Class<?> guessClassByName(final String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("Could not guess class during page deserialization, falling back to Map", e);
            return Map.class;
        }
    }
}
