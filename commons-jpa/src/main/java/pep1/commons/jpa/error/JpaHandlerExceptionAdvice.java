/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.jpa.error;

import pep1.commons.domain.message.ErrorMessage;
import pep1.commons.domain.message.ValidationErrorMessage;
import pep1.commons.error.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@Slf4j
@ControllerAdvice
public class JpaHandlerExceptionAdvice {

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorMessage> illegalArgument(DataIntegrityViolationException e) {
        log.error("Illegal argument: " + e.getLocalizedMessage());

        if (e.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
            return validationFailed((org.hibernate.exception.ConstraintViolationException) e.getCause());
        }

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.INVALID_INPUT,
                e.getLocalizedMessage()
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
    public ResponseEntity<ErrorMessage> validationFailed(org.hibernate.exception.ConstraintViolationException e) {
        log.error("Constraint violation: " + e.getLocalizedMessage());

        ValidationErrorMessage message = new ValidationErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.VALIDATION_FAILED,
                "Validation of sent resource failed. Please check input."
        );

        message.addError(e.getConstraintName(), null, e.getLocalizedMessage());

        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TransactionSystemException.class)
    public ResponseEntity<ErrorMessage> transactionFailed(TransactionSystemException e) {
        log.error("Constraint violation: " + e.getLocalizedMessage());

        if (e.getRootCause() instanceof ConstraintViolationException) {
            return validationFailed((ConstraintViolationException) e.getRootCause());
        }

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.INVALID_INPUT,
                e.getLocalizedMessage()
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorMessage> validationFailed(ConstraintViolationException e) {
        log.error("Constraint violation: " + e.getLocalizedMessage());

        ValidationErrorMessage message = new ValidationErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.VALIDATION_FAILED,
                "Validation of sent resource failed. Please check input."
        );

        for (ConstraintViolation violation : e.getConstraintViolations()) {
            log.warn("Constraint violation: {}", violation.getMessage());
            message.addError(violation.getPropertyPath().toString(), violation.getLeafBean(), violation.getMessage());
        }

        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ErrorMessage> validationFailed(BindException e) {
        log.error("Bind exception: " + e.getLocalizedMessage());

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.INVALID_INPUT,
                e.getAllErrors().toString()
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PropertyReferenceException.class)
    public ResponseEntity<ErrorMessage> wrongReference(PropertyReferenceException e) {
        log.error("Bind exception: " + e.getLocalizedMessage());

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.INVALID_INPUT,
                e.getMessage()
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorMessage> notFound(EntityNotFoundException e) {
        String message = (StringUtils.hasText(e.getLocalizedMessage()))
                ? e.getLocalizedMessage()
                : "The requested resource was not found.";
        log.error("Not Found Exception: " + message);

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.NOT_FOUND,
                ErrorCodes.NOT_FOUND,
                message
        ), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EntityExistsException.class)
    public ResponseEntity<ErrorMessage> existsAlready(EntityExistsException e) {
        String message = (StringUtils.hasText(e.getLocalizedMessage()))
                ? e.getLocalizedMessage()
                : "A resource with given parameters exists already";
        log.error("Not Found Exception: " + message);

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.CONFLICT,
                ErrorCodes.EXISTS_ALREADY,
                message
        ), HttpStatus.CONFLICT);
    }
}
