/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.jpa.util;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import org.springframework.util.NumberUtils;

import java.io.Serializable;
import java.util.Properties;

@Slf4j
public class RandomIntIdGenerator implements IdentifierGenerator, Configurable {

    public static final String LENGTH_CONFIG_KEY = "length";

    private static final Integer DEFAULT_LENGTH = 12;

    private Long min;
    private Long max;

    @Override
    public void configure(Type type, Properties properties, ServiceRegistry serviceRegistry) throws MappingException {
        Integer length = DEFAULT_LENGTH;
        try {
            Integer configuredLength = NumberUtils.parseNumber(properties.getProperty(LENGTH_CONFIG_KEY), Integer.class);
            length = (configuredLength != null) ? configuredLength : DEFAULT_LENGTH;
        } catch (NumberFormatException | NullPointerException e) {
            log.warn("Error while parsing configuration", e);
            // do nothing
        }

        min = NumberUtils.parseNumber("1" + new String(new char[length - 1]).replace('\0', '0'), Long.class);
        max = NumberUtils.parseNumber("9" + new String(new char[length - 1]).replace('\0', '0'), Long.class);

        if (log.isDebugEnabled()) {
            log.info("length: {}", length);
            log.info("min: {}", min);
            log.info("max: {}", max);
        }
    }

    @Override
    public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws HibernateException {
        if (log.isDebugEnabled()) {
            log.debug("Invoking generate id method for object {}.", o);
        }

        return (long) Math.floor(Math.random() * max) + min;
    }
}
