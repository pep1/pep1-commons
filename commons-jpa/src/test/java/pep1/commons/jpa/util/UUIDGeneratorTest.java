/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.jpa.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import pep1.commons.jpa.domain.AbstractPersistable;

import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

/**
 * created on 12/6/16
 */
@Slf4j
public class UUIDGeneratorTest {

    private AbstractPersistable persistentObject = new AbstractPersistable() {};


    @Test
    public void generate() throws Exception {
        UUIDGenerator generator = new UUIDGenerator();

        String[] uuidArray = new String[200];

        IntStream.range(0, uuidArray.length).parallel().forEach(
                num -> uuidArray[num] = (generator.generate(null, persistentObject)).toString()
        );

        assertTrue(areValuesUnique(uuidArray));
    }

    private static Boolean areValuesUnique(String[] values) {
        for (int i = 0; i < values.length; ++i) {
            for (int j = i + 1; j < values.length; ++j) {
                if (values[i].equals(values[j])) {
                    return false;
                }
            }
        }
        return true;
    }

}