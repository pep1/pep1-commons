/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import pep1.commons.jackson.View;

import java.util.List;

public class PageDTO extends AbstractDTO {

    @JsonIgnore
    private Page page;

    public PageDTO(@NonNull final Page page) {
        this.page = page;
    }

    @JsonProperty("content")
    @JsonView(View.Page.class)
    public List getContent() {
        return page.getContent();
    }

    @JsonProperty("meta")
    @JsonView(View.Page.class)
    public MetaDataDTO getMeta() {
        MetaDataDTO meta = new MetaDataDTO();
        meta.setIsFirst(page.isFirst());
        meta.setIsLast(page.isLast());
        meta.setPage(page.getNumber());
        meta.setPerPage(page.getSize());
        meta.setTotalPages(page.getTotalPages());
        meta.setTotalItems(page.getTotalElements());

        // define content type
        String contentType;
        List<?> content = page.getContent();

        if (content.size() > 0) {
            contentType = content.get(0).getClass().getName();
        } else {
            contentType = "UNKNOWN";
        }

        meta.setContentType(contentType);
        return meta;
    }
}
