/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pep1.commons.domain.dto.AbstractDTO;
import pep1.commons.jackson.View;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class MetaDataDTO extends AbstractDTO {

    @JsonProperty("page")
    @JsonView(View.Page.class)
    private Integer page;

    @JsonProperty("page_size")
    @JsonView(View.Page.class)
    private Integer perPage;

    @JsonProperty("total_pages")
    @JsonView(View.Page.class)
    private Integer totalPages;

    @JsonProperty("total_items")
    @JsonView(View.Page.class)
    private Long totalItems;

    @JsonProperty("is_first")
    @JsonView(View.Page.class)
    private Boolean isFirst;

    @JsonProperty("is_last")
    @JsonView(View.Page.class)
    private Boolean isLast;

    @JsonProperty("content_type")
    @JsonView(View.Page.class)
    private String contentType;

}
