/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.domain.message;

import net.minidev.json.annotate.JsonIgnore;
import org.springframework.http.HttpStatus;
import pep1.commons.error.ValidationError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationErrorMessage extends ErrorMessage {

    private final Map<String, List<ValidationError>> errors = new HashMap<>();

    public ValidationErrorMessage(HttpStatus status, String errorCode, String message) {
        super(status, errorCode, message);
    }

    public Map<String, List<ValidationError>> getErrors() {
        return errors;
    }

    @JsonIgnore
    public ValidationErrorMessage addError(String field, Object rejectedValue, String message) {
        ValidationError error = new ValidationError();
        error.setRejected(rejectedValue);
        error.setMessage(message);

        addError(field, error);

        return this;
    }

    @JsonIgnore
    public ValidationErrorMessage addError(String field, ValidationError error) {
        if (!errors.containsKey(field)) {
            errors.put(field, new ArrayList<>());
        }

        errors.get(field).add(error);

        return this;
    }
}
