/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.hashid;

import lombok.NonNull;
import org.hashids.Hashids;
import org.springframework.util.StringUtils;

/**
 * Hash Id utility class.
 */
public final class HashIdService {

    /**
     * Min Hash length.
     */
    private static final int DEFAULT_MIN_HASH_LENGTH = 6;

    /**
     * Internal properties.
     */
    private final Hashids instance;

    public HashIdService(@NonNull String salt, Integer minHashLength) {
        this.instance = new Hashids(salt, (minHashLength != null) ? minHashLength : DEFAULT_MIN_HASH_LENGTH);
    }

    /**
     * Encrypt the given long value to a hash.
     *
     * @param value the long value.
     * @return the hash.
     */
    public String encrypt(final Long value) {
        if (value == null) {
            return null;
        }
        return instance.encode(value);
    }

    /**
     * Decrypt the given string to the original long value.
     *
     * @param value the decrypted hash.
     * @return the long value.
     */
    public Long decrypt(final String value) {
        if (!StringUtils.hasLength(value)) {
            return null;
        }

        long[] result = instance.decode(value);
        return (result != null && result.length == 1) ? result[0] : null;
    }
}
