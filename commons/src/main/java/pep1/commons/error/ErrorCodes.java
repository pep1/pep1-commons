/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.error;



public final class ErrorCodes {

    private ErrorCodes() {
        // NOP
    }
    //CHECKSTYLE:OFF
    public static final String INVALID_INPUT = "invalid_input";
    public static final String VALIDATION_FAILED = "validation_failed";

    public static final String USER_EXISTS = "user_exists";
    public static final String EMAIL_EXISTS = "email_exists";
    public static final String INTERNAL_ERROR = "internal_error";
    public static final String UPLOAD_FAILED = "upload_failed";

    public static final String BAD_REQUEST = "bad_request";
    public static final String NOT_FOUND = "not_found";
    public static final String EXISTS_ALREADY = "exists_already";

    public static final String UNAUTHORIZED = "unauthorized";
    public static final String ACCESS_DENIED = "access_denied";
    public static final String ACCESS_TOKEN_EXPIRED = "access_token_expired";
    public static final String ACCESS_TOKEN_INVALID = "access_token_invalid";
    //CHECKSTYLE:ON
}
