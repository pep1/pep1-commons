/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.util;

import com.google.common.collect.ObjectArrays;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.PropertyAccessorFactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Utility class for copy object properties.
 */
@UtilityClass
public final class CopyUtils {

    /**
     * Copies all not null properties from src object to target.
     *
     * @param src    the source object.
     * @param target the target object.
     */
    public static void copyNonNullProperties(final Object src, final Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    /**
     * Copies all specified not null properties from src object to target.
     *
     * @param src         the source object.
     * @param target      the target object.
     * @param propsToCopy property names to copy (whitelist principle).
     */
    public static void copyNonNullProperties(
            @NonNull final Object src,
            @NonNull final Object target,
            @NonNull final String... propsToCopy) {
        BeanWrapper srcWrap = PropertyAccessorFactory.forBeanPropertyAccess(src);
        BeanWrapper trgWrap = PropertyAccessorFactory.forBeanPropertyAccess(target);

        List<String> nullProperties = Arrays.asList(getNullPropertyNames(src));

        // filter the null values from props to copy
        Arrays.stream(propsToCopy).filter(s -> !nullProperties.contains(s))
                // copy into target bean
                .forEach(prop -> trgWrap.setPropertyValue(prop, srcWrap.getPropertyValue(prop)));
    }

    /**
     * Copies all not null properties from src object to target, except specified properties.
     *
     * @param src            the source object.
     * @param target         the target object.
     * @param propsNotToCopy property names not to copy (blacklist principle).
     */
    public static void copyNonNullPropertiesExcept(
            @NonNull final Object src,
            @NonNull final Object target,
            final String... propsNotToCopy) {
        BeanUtils.copyProperties(src, target, ObjectArrays.concat(getNullPropertyNames(src), propsNotToCopy, String.class));
    }

    /**
     * Gets all null value property names of an object.
     *
     * @param source the source object.
     * @return the null value property names
     */
    private static String[] getNullPropertyNames(@NonNull final Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
