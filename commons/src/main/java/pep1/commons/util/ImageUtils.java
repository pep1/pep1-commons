/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * ImageUtils helper class.
 */
@Slf4j
@UtilityClass
public final class ImageUtils {

    /**
     * Supported media types for images.
     */
    public static final List<String> SUPPORTED_MIMETYPES = Arrays.asList(
            "image/png",
            "image/jpeg"
    );

    /**
     * Checks a given image input stream and content type for validity.
     *
     * @param stream      the image input stream.
     * @param contentType the image content type.
     * @return true if image is valid
     */
    public static Boolean check(final InputStream stream, final String contentType) {
        try {
            // check mime type and try to read image
            return
                    StringUtils.hasText(contentType)
                            && SUPPORTED_MIMETYPES.contains(contentType.toLowerCase())
                            && (ImageIO.read(stream) != null);
        } catch (IOException e) {
            // It's not an image.
            return false;
        }
    }

    /**
     * Checks a given multipart file is a valid image.
     *
     * @param file the upload file.
     * @return true if file is a valid image.
     */
    public static Boolean check(final MultipartFile file) {
        try {
            return check(file.getInputStream(), file.getContentType());
        } catch (IOException e) {
            return false;
        }
    }
}
