/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.util;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.springframework.util.Base64Utils;

import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * StringUtils utility class.
 */
@UtilityClass
public final class StringUtils {

    /**
     * UUID regex pattern.
     */
    private static final Pattern UUID_PATTERN = Pattern.compile(
            "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}");

    /**
     * Checks if a given String is a UUID.
     *
     * @param toCheck the given string.
     * @return true, if a valid UUID.
     */
    public static Boolean isUUID(@NonNull final String toCheck) {
        return null != toCheck && UUID_PATTERN.matcher(toCheck).matches();
    }

    /**
     * Tries to parse a given String input to a primitive, double, boolean.
     *
     * @param input the given string.
     * @return the primitive object.
     */
    public static Object parseToPrimitive(@NonNull final String input) {
        try {
            return Double.parseDouble(input);
        } catch (NumberFormatException nfe) {
            // proceed
        }

        if ("true".equalsIgnoreCase(input) || "false".equalsIgnoreCase(input)) {
            return Boolean.parseBoolean(input);
        }

        return input;
    }
}
