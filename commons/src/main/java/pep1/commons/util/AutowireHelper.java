/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Optional;

/**
 * Autowiring helper for static bean injection.
 */
@Slf4j
public class AutowireHelper implements ApplicationContextAware {

    private static ApplicationContext APPLICATION_CONTEXT;

    /**
     * Tries to autowire the specified instance of the class if one of the specified beans which need to be autowired
     * are null.
     *
     * @param classToAutowire        the instance of the class which holds @Autowire annotations
     * @param beansToAutowireInClass the beans which have the @Autowire annotation in the specified {#classToAutowire}
     */
    public static void autowire(Object classToAutowire, Object... beansToAutowireInClass) {
        for (Object bean : beansToAutowireInClass) {
            if (bean == null) {
                APPLICATION_CONTEXT.getAutowireCapableBeanFactory().autowireBean(classToAutowire);
            }
        }
    }

    /**
     * Sets the application context.
     *
     * @param applicationContext the application context.
     */
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        AutowireHelper.APPLICATION_CONTEXT = applicationContext;
    }

    /**
     * Gets the singleton application context instance.
     *
     * @return the singleton instance.
     */
    public static Optional<ApplicationContext> getContext() {
        return Optional.ofNullable(APPLICATION_CONTEXT);
    }

    /**
     * Gets a dedicated bean by class.
     *
     * @param clazz the class to get.
     * @param <T>   the return type.
     * @return Optional of the bean.
     */
    public static <T> Optional<T> getBean(Class<T> clazz) {
        return getContext().map(applicationContext -> applicationContext.getBean(clazz));
    }

}