/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage;

import com.google.common.io.Files;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;
import pep1.commons.storage.config.properties.LocalBinaryStorageProperties;
import pep1.commons.storage.domain.Storable;
import pep1.commons.storage.exception.StorageException;
import pep1.commons.storage.service.BinaryStorage;
import pep1.commons.storage.service.LocalStorage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LocalBinaryStorageTest {

    @ClassRule
    public final static TemporaryFolder tempFolder = new TemporaryFolder();

    private BinaryStorage binaryStorage;

    private ClassPathResource resource = new ClassPathResource(TestStorable.TEST_FILE_PATH);

    private TestStorable testStorable = new TestStorable();

    private LocalBinaryStorageProperties properties;

    @Before
    public void setup() {
        properties = new LocalBinaryStorageProperties();
        properties.setEnabled(true);
        properties.setPort(8080);
        properties.setHostname("localhost");
        properties.setStoragePath(tempFolder.getRoot().getAbsolutePath());
        properties.setPath("/media");
        properties.setProtocol("http");

        binaryStorage = new LocalStorage(properties);
    }

    @Test
    public void testStorageIsThere() {
        assertNotNull(binaryStorage);
    }

    @Test
    public void testResourceToSaveExists() {
        assertTrue(resource.exists());
    }

    @Test
    public void testStoreFile() throws IOException {
        binaryStorage.persistStorable(testStorable, resource.getInputStream());
        // ensure that file is there
        assertTrue(
                new File(tempFolder.getRoot().getAbsolutePath() + File.separator +
                        testStorable.getStorableType() + File.separator +
                        testStorable.getFileName())
                        .exists());
    }

    @Test
    public void testStoreAndRemoveFile() throws IOException {
        binaryStorage.persistStorable(testStorable, resource.getInputStream());
        // ensure that file is there
        assertTrue(
                new File(tempFolder.getRoot().getAbsolutePath() + File.separator +
                        testStorable.getStorableType() + File.separator +
                        testStorable.getFileName())
                        .exists());

        binaryStorage.removeStorable(testStorable);

        // ensure that file is not there anymore
        assertTrue(
                !new File(tempFolder.getRoot().getAbsolutePath() + File.separator +
                        testStorable.getStorableType() + File.separator +
                        testStorable.getFileName())
                        .exists());
    }

    @Test
    public void testGetStreamFromFile() throws IOException {
        binaryStorage.persistStorable(testStorable, resource.getInputStream());
        // ensure that file is there
        InputStream stream = binaryStorage.getStreamFromStorable(testStorable);

        assertTrue(stream instanceof FileInputStream);

        File tempFile = tempFolder.newFile();
        FileCopyUtils.copy(stream, new FileOutputStream(tempFile));

        assertTrue(Files.equal(resource.getFile(), tempFile));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullArgumentFails() {
        binaryStorage.enhanceStorable(null);
    }

    @Test(expected = StorageException.class)
    public void testNotExistentFileStreamFails() throws StorageException {
        binaryStorage.getStreamFromStorable(new Storable() {
            @Override
            public String getFileName() {
                return "not_existent_file";
            }

            @Override
            public String getStorableType() {
                return "whatever";
            }

            @Override
            public String getUrl() {
                return "whatever";
            }

            @Override
            public void setUrl(String url) {

            }
        });
    }

    @Test(expected = StorageException.class)
    public void testNotReadableFilePersistFails() throws IOException {
        InputStream stream = mock(InputStream.class);
        when(stream.read(any())).thenThrow(new IOException());

        binaryStorage.persistStorable(
                new Storable() {
                    @Override
                    public String getFileName() {
                        return "not_there";
                    }

                    @Override
                    public String getStorableType() {
                        return "whatever";
                    }

                    @Override
                    public String getUrl() {
                        return "whatever";
                    }

                    @Override
                    public void setUrl(String url) {

                    }
                },
                stream);
    }
}
