/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import pep1.commons.storage.domain.Storable;
import pep1.commons.storage.service.BinaryStorage;

import java.util.Collection;

@Aspect
@Slf4j
public class StorableEnhancer {

    private final BinaryStorage storage;

    @Autowired
    public StorableEnhancer(BinaryStorage storage) {
        this.storage = storage;
    }

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void beanAnnotatedWithRestController() {
    }

    @Pointcut("execution(public * *(..))")
    public void publicMethod() {
    }

    @Pointcut("publicMethod() && beanAnnotatedWithRestController()")
    public void allRestControllerMethods() {
    }

    @Pointcut("@annotation(pep1.commons.storage.aspect.EnhanceStorable)")
    public void methodWithAnnotation() {
    }


    @AfterReturning(
            pointcut = "methodWithAnnotation() || allRestControllerMethods()",
            returning = "retVal")
    @SuppressWarnings("unchecked")
    public void enhanceObjects(Object retVal) {

        if (retVal instanceof Storable) {
            log.debug("Enhancing single storable: {}", retVal);
            storage.enhanceStorable((Storable) retVal);
        }

        if (retVal instanceof Page) {
            Page page = (Page) retVal;
            if (page.hasContent() && page.getContent().get(0) instanceof Storable) {
                log.debug("Enhancing paged storable: {}", retVal);
                page.getContent().forEach(o -> storage.enhanceStorable((Storable) o));
            }
        }

        if (retVal instanceof Collection) {
            Collection collection = (Collection) retVal;
            if (!collection.isEmpty()) {
                log.debug("Enhancing paged storable: {}", retVal);
                collection.forEach(o -> {
                    if (o instanceof Storable) {
                        storage.enhanceStorable((Storable) o);
                    }
                });
            }
        }
    }
}
