/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.Assert;
import pep1.commons.storage.aspect.StorableEnhancer;
import pep1.commons.storage.config.properties.CloudinaryProperties;
import pep1.commons.storage.config.properties.LocalBinaryStorageProperties;
import pep1.commons.storage.config.properties.S3BinaryStorageProperties;
import pep1.commons.storage.config.properties.StorageProperties;
import pep1.commons.storage.s3.SimpleStorageResourceLoader;
import pep1.commons.storage.service.BinaryStorage;
import pep1.commons.storage.service.CloudinaryStorage;
import pep1.commons.storage.service.LocalStorage;
import pep1.commons.storage.service.S3Storage;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
@Slf4j
@ConditionalOnProperty("storage.enabled")
@EnableConfigurationProperties(StorageProperties.class)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class StorageConfig {

    @Bean
    public StorableEnhancer storableEnhancer(BinaryStorage binaryStorage) {
        return new StorableEnhancer(binaryStorage);
    }

    @Bean
    @ConditionalOnProperty("storage.local.enabled")
    public BinaryStorage localBinaryStorage(StorageProperties properties, Environment environment) {
        LocalBinaryStorageProperties localStorageProperties = properties.getLocal();
        Assert.notNull(localStorageProperties, "Local storage properties may not be null when enabled");

        if (localStorageProperties.getHostname() == null) {
            try {
                localStorageProperties.setHostname(InetAddress.getLocalHost().getHostName());
                log.info("Guessed hostname: '{}'", localStorageProperties.getHostname());
            } catch (UnknownHostException e) {
                log.warn("Could not get hostname, defaulting to 'localhost'");
                localStorageProperties.setHostname("localhost");
            }
        }

        if (localStorageProperties.getPort() == null) {
            localStorageProperties.setPort(Integer.valueOf(environment.getProperty("local.properties.port")));
        }

        return new LocalStorage(localStorageProperties);
    }

    @Bean
    @ConditionalOnProperty("storage.s3.enabled")
    @ConditionalOnMissingBean
    public AmazonS3 amazonS3(StorageProperties properties) {
        S3BinaryStorageProperties s3Properties = properties.getS3();
        Assert.notNull(s3Properties, "S3 storage properties may not be null when enabled");

        BasicAWSCredentials credentials = new BasicAWSCredentials(
                s3Properties.getAccessKey(),
                s3Properties.getSecretKey());

        final AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(s3Properties.getEndpointUrl(), s3Properties.getRegion()))
                .withCredentials(new AWSStaticCredentialsProvider(credentials));
        builder.setPathStyleAccessEnabled(s3Properties.getPathStyleAccessEnabled());

        return builder.build();
    }

    @Bean
    @ConditionalOnProperty("storage.s3.enabled")
    public BinaryStorage s3BinaryStorage(StorageProperties properties, AmazonS3 amazonS3) {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setBeanName("S3TaskExecutor");
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(5);
        taskExecutor.setQueueCapacity(10);
        taskExecutor.setAllowCoreThreadTimeOut(true);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setKeepAliveSeconds(120);
        taskExecutor.afterPropertiesSet();

        S3BinaryStorageProperties s3Properties = properties.getS3();
        SimpleStorageResourceLoader resourceLoader = new SimpleStorageResourceLoader(amazonS3);
        resourceLoader.setTaskExecutor(taskExecutor);

        return new S3Storage(s3Properties.getEndpointUrl(), s3Properties.getBucket(), resourceLoader);
    }

    @Bean
    @ConditionalOnProperty("storage.cloudinary.enabled")
    public BinaryStorage cloudinaryStorage(StorageProperties properties) {
        CloudinaryProperties cloudinaryProperties = properties.getCloudinary();
        return new CloudinaryStorage(cloudinaryProperties);
    }
}
