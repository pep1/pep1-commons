/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.service;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.FileCopyUtils;
import pep1.commons.storage.domain.Storable;
import pep1.commons.storage.config.properties.LocalBinaryStorageProperties;
import pep1.commons.storage.exception.StorageException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Local binary storage implementation.
 * <p>
 * Good for local testing and development.
 * </p>
 */
@Slf4j
public class LocalStorage implements BinaryStorage {

    private Integer port;

    private String basePath;

    private String hostName;

    private String path;

    private String protocol;

    public LocalStorage(@NonNull LocalBinaryStorageProperties properties) {
        this.port = properties.getPort();
        this.basePath = properties.getStoragePath();
        this.hostName = properties.getHostname();
        this.path = properties.getPath();
        this.protocol = properties.getProtocol();
    }

    @Override
    public Storable persistStorable(@NonNull Storable storable, @NonNull InputStream stream) throws StorageException {
        String storablePath = getAbsolutePath(storable);

        log.debug("storing storable {} ", storable.getFileName());
        log.debug("to: {}", storablePath);

        try {

            if (new File(storablePath).mkdirs()) {
                log.debug("Created new folder to store files");
            }

            // new file where the storable file should be created
            File newFile = new File(storablePath);

            // overwrite
            if (newFile.exists() && newFile.delete()) {
                log.info("File overwritten: {}", newFile);
            }

            FileCopyUtils.copy(stream, new FileOutputStream(newFile));
        } catch (IOException | SecurityException e) {
            throw new StorageException("Error while storing binary: " + storablePath, e);
        }

        return storable;
    }

    @Override
    public InputStream getStreamFromStorable(@NonNull Storable storable) throws StorageException {
        String filePath = getAbsolutePath(storable);

        try {
            return new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            throw new StorageException("Error while reading file: " + filePath, e);
        }
    }

    @Override
    public void removeStorable(@NonNull Storable storable) throws StorageException {
        String filePath = getAbsolutePath(storable);

        if (!new File(filePath).delete()) {
            throw new StorageException("Could not delete storable file");
        }
    }

    @Override
    public void enhanceStorable(@NonNull Storable storable) {
        try {
            storable.setUrl(
                    new URL(
                            protocol,
                            hostName,
                            port,
                            path + "/" + storable.getFileName())
                            .toString()
            );
        } catch (MalformedURLException e) {
            log.error("Could not build uri!", e);
        }
    }

    private String getStorablePath(@NonNull Storable storable) {
        return basePath + File.separator + storable.getStorableType();
    }

    private String getAbsolutePath(@NonNull Storable storable) {
        return getStorablePath(storable) + File.separator + storable.getFileName();
    }
}
