/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.service;

import pep1.commons.storage.domain.Storable;
import pep1.commons.storage.exception.StorageException;

import java.io.InputStream;

/**
 * A Storage for persisting binary data to any kind of storage.
 */
public interface BinaryStorage {

    /**
     * An object from type <code>Storable</code> and given <code>InputStream</code> is stored in the internal
     * storage.
     *
     * @param storable the entity that should be stored. Filename and Storable should be set.
     * @return the stored storable.
     */
    Storable persistStorable(Storable storable, InputStream stream) throws StorageException;


    /**
     * Gets the input stream of a stored <code>Storable</code>.
     *
     * @param storable the given storable.
     * @return the input stream.
     */
    InputStream getStreamFromStorable(Storable storable) throws StorageException;

    /**
     * Deletes the given storable from the store.
     *
     * @param storable the storable to delete.
     */
    void removeStorable(Storable storable) throws StorageException;

    /**
     * Enriches the storable object with additional, transient information during api serialization.
     * E.g. public storable uri.
     *
     * @param storable the storable object.
     */
    void enhanceStorable(Storable storable);
}
