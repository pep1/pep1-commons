/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.service;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.google.common.collect.ImmutableMap;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import pep1.commons.storage.config.properties.CloudinaryProperties;
import pep1.commons.storage.config.properties.thumbs.ImageProperty;
import pep1.commons.storage.domain.Storable;
import pep1.commons.storage.domain.ThumbnailDTO;
import pep1.commons.storage.domain.Thumbnailable;
import pep1.commons.storage.exception.StorageException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class CloudinaryStorage implements BinaryStorage {

    public static final String JPG_EXTENSION = "jpg";
    public static final String DEFAULT_THUMBS_KEY = "DEFAULT";

    private final Cloudinary client;

    private final Map<String, List<ImageProperty>> thumbProperties;

    private final List<ImageProperty> defaultThumbs;

    private final ImageProperty uploadTransformation;

    private final String subFolder;

    private final Boolean secure;

    private final Integer bufferSize;

    public CloudinaryStorage(@NonNull CloudinaryProperties properties) {
        Assert.notNull(properties, "Cloudinary storage properties may not be null");
        Assert.notNull(properties.getProperties(), "Properties may not be null");
        Assert.notNull(properties.getThumbs(), "Thumbnail settings may not be null");
        Assert.notNull(properties.getThumbs().get(DEFAULT_THUMBS_KEY), "Default thumbs key may not be null");

        Map<String, String> options = ImmutableMap.<String, String>builder()
            .putAll(properties.getProperties())
            .put("cloud_name", properties.getCloudName())
            .put("api_key", properties.getApiKey())
            .put("api_secret", properties.getApiSecret())
            .build();

        client = new Cloudinary(options);

        this.thumbProperties = properties.getThumbs();
        this.defaultThumbs = properties.getThumbs().get(DEFAULT_THUMBS_KEY);
        this.uploadTransformation = properties.getUploadTransform();
        this.subFolder = org.apache.commons.lang3.StringUtils.stripStart(
            org.apache.commons.lang3.StringUtils.stripEnd(
                properties.getSubFolder(),
                "/"
            ),
            "/"
        );

        this.secure = properties.getSecure();
        this.bufferSize = properties.getBufferSize();
    }

    @Override
    public Storable persistStorable(@NonNull Storable storable, @NonNull InputStream stream) throws StorageException {
        try {
            String tag = getTag(storable);

            Map response = client.uploader().uploadLarge(stream, ObjectUtils.asMap(
                "resource_type", "auto",
                "use_filename", "false",
                "public_id", StringUtils.stripFilenameExtension(storable.getFileName()),
                "folder", getFolder(storable),
                "tag", tag,
                "transformation", new Transformation()
                    .width(uploadTransformation.getWidth())
                    .height(uploadTransformation.getHeight())
                    .crop(uploadTransformation.getCrop())
            ), bufferSize);

            log.debug("Upload response: ", response);
            return storable;
        } catch (IOException e) {
            throw new StorageException("Could not upload storable to Cloudinary", e);
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                log.error("Could not close the input stream!");
            }
        }
    }

    @Override
    public InputStream getStreamFromStorable(@NonNull Storable storable) throws StorageException {
        throw new StorageException("Getting stream from storable not implemented in Cloudinary implementation");
    }

    @Override
    public void removeStorable(@NonNull Storable storable) throws StorageException {
        try {
            Map response = client.uploader().destroy(getPublicId(storable), ObjectUtils.emptyMap());
            log.debug("Remove response: ", response);
        } catch (IOException e) {
            throw new StorageException("Could not delete storable in Cloudinary", e);
        }
    }

    @Override
    public void enhanceStorable(@NonNull Storable storable) {
        if (StringUtils.hasText(storable.getUrl())) {
            log.debug("Storable {} already has an url set, skipping", storable);
            return;
        }

        storable.setUrl(
            client.url()
                .source(getPublicId(storable))
                .signed(true)
                .secure(secure)
                .generate()
        );

        if (storable instanceof Thumbnailable) {
            Thumbnailable thumbnailable = (Thumbnailable) storable;
            Map<String, ThumbnailDTO> thumbs = new LinkedHashMap<>();

            List<ImageProperty> thumbList = thumbProperties.getOrDefault(
                thumbnailable.getThumbType(),
                defaultThumbs
            );

            thumbList.forEach(
                imageProperty -> thumbs.put(
                    imageProperty.toKey(),
                    ThumbnailDTO.builder()
                        .url(client.url()
                            .source(getPublicId(storable) + "." + JPG_EXTENSION)
                            .transformation(
                                new Transformation()
                                    .width(imageProperty.getWidth())
                                    .height(imageProperty.getHeight())
                                    .crop(imageProperty.getCrop())
                                    .gravity(imageProperty.getGravity())
                                    .fetchFormat("auto")
                            )
                            .signed(true)
                            .secure(secure)
                            .generate()
                        )
                        .build()
                ));

            thumbnailable.setThumbs(thumbs);
        }
    }

    public String getTag(@NonNull Storable storable) {
        String tag;
        if (storable instanceof Thumbnailable) {
            tag = ((Thumbnailable) storable).getThumbType();
        } else {
            tag = "BINARY";
        }
        return tag;
    }

    public String getFolder(@NonNull Storable storable) {
        if (StringUtils.hasText(this.subFolder)) {
            return subFolder + "/" + storable.getStorableType();
        } else {
            return storable.getStorableType();
        }
    }

    public String getPublicId(@NonNull Storable storable) {
        return getFolder(storable) + "/" + StringUtils.stripFilenameExtension(storable.getFileName());
    }

    public void cleanFolder(@NonNull String folderName) throws StorageException {
        try {
            client.api().deleteResourcesByPrefix(folderName, Collections.emptyMap());
        } catch (Exception e) {
            log.error("Could not delete resources from folder!", e);
            throw new StorageException("Could not delete resources from folder!", e);
        }
    }
}
