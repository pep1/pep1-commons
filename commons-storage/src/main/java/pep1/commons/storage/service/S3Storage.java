/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.service;

import com.amazonaws.util.IOUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.WritableResource;
import pep1.commons.storage.domain.Storable;
import pep1.commons.storage.exception.StorageException;
import pep1.commons.storage.s3.SimpleStorageResource;
import pep1.commons.storage.s3.SimpleStorageResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Slf4j
public class S3Storage implements BinaryStorage {

    private static final String PATH_PREFIX = "s3://";

    private final String endpointUrl;
    private final String bucket;

    private final SimpleStorageResourceLoader resourceLoader;

    public S3Storage(String endpointUrl, String bucket, SimpleStorageResourceLoader resourceLoader) {
        this.endpointUrl = endpointUrl;
        this.resourceLoader = resourceLoader;
        this.bucket = bucket;
    }

    @Override
    public Storable persistStorable(@NonNull Storable storable, @NonNull InputStream stream) throws StorageException {
        String s3Path = getS3Path(storable);
        try {
            WritableResource resource = (WritableResource) resourceLoader.getResource(s3Path);
            try (OutputStream outputStream = resource.getOutputStream()) {
                IOUtils.copy(stream, outputStream);
            }
            return storable;
        } catch (ClassCastException | IOException e) {
            throw new StorageException("Could not write S3 blob: " + s3Path, e);
        }
    }

    @Override
    public InputStream getStreamFromStorable(@NonNull Storable storable) throws StorageException {
        String s3Path = getS3Path(storable);
        try {
            return resourceLoader.getResource(s3Path).getInputStream();
        } catch (IOException e) {
            throw new StorageException("Could not write S3 blob: " + s3Path, e);
        }
    }

    @Override
    public void removeStorable(@NonNull Storable storable) throws StorageException {
        String s3Path = getS3Path(storable);
        try {
            SimpleStorageResource resource = (SimpleStorageResource) resourceLoader.getResource(s3Path);
            resource.delete();
        } catch (ClassCastException | IOException e) {
            throw new StorageException("Could not remove S3 blob: " + s3Path, e);
        }
    }

    @Override
    public void enhanceStorable(@NonNull Storable storable) {
        String s3Path = getS3Path(storable);
        SimpleStorageResource resource = (SimpleStorageResource) resourceLoader.getResource(s3Path);
        try {
            storable.setUrl(resource.getURL().toString());
        } catch (IOException e) {
            log.error("Could not build URL for S3 storable!", e);
        }
    }


    private String getS3Path(@NonNull Storable storable) {
        return PATH_PREFIX + bucket + "/" + storable.getStorableType() + "/" + storable.getFileName();
    }
}
