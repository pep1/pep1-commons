/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.s3;

import org.springframework.util.Assert;

/**
 * Utility class that provides utility method to work with s3 storage resources
 *
 * @author Agim Emruli
 * @author Alain Sahli
 * @since 1.0
 */
final class SimpleStorageNameUtils {

    private static final String S3_PROTOCOL_PREFIX = "s3://";
    private static final String PATH_DELIMITER = "/";
    private static final String VERSION_DELIMITER = "^";

    private SimpleStorageNameUtils() {
        // Avoid instantiation
    }

    static boolean isSimpleStorageResource(String location) {
        Assert.notNull(location, "Location must not be null");
        return location.toLowerCase().startsWith(S3_PROTOCOL_PREFIX);
    }

    static String getBucketNameFromLocation(String location) {
        Assert.notNull(location, "Location must not be null");
        if (!isSimpleStorageResource(location)) {
            throw new IllegalArgumentException("The location :'" + location + "' is not a valid S3 location");
        }
        int bucketEndIndex = location.indexOf(PATH_DELIMITER, S3_PROTOCOL_PREFIX.length());
        if (bucketEndIndex == -1 || bucketEndIndex == S3_PROTOCOL_PREFIX.length()) {
            throw new IllegalArgumentException("The location :'" + location + "' does not contain a valid bucket name");
        }
        return location.substring(S3_PROTOCOL_PREFIX.length(), bucketEndIndex);
    }

    static String getObjectNameFromLocation(String location) {
        Assert.notNull(location, "Location must not be null");
        if (!isSimpleStorageResource(location)) {
            throw new IllegalArgumentException("The location :'" + location + "' is not a valid S3 location");
        }
        int bucketEndIndex = location.indexOf(PATH_DELIMITER, S3_PROTOCOL_PREFIX.length());
        if (bucketEndIndex == -1 || bucketEndIndex == S3_PROTOCOL_PREFIX.length()) {
            throw new IllegalArgumentException("The location :'" + location + "' does not contain a valid bucket name");
        }

        if (location.contains(VERSION_DELIMITER)) {
            return getObjectNameFromLocation(location.substring(0, location.indexOf(VERSION_DELIMITER)));
        }

        if (location.endsWith(PATH_DELIMITER)) {
            return location.substring(++bucketEndIndex, location.length() - 1);
        }

        return location.substring(++bucketEndIndex, location.length());
    }

    static String getVersionIdFromLocation(String location) {
        Assert.notNull(location, "Location must not be null");
        if (!isSimpleStorageResource(location)) {
            throw new IllegalArgumentException("The location :'" + location + "' is not a valid S3 location");
        }
        int objectNameEndIndex = location.indexOf(VERSION_DELIMITER, S3_PROTOCOL_PREFIX.length());
        if (objectNameEndIndex == -1 || location.endsWith(VERSION_DELIMITER)) {
            return null;
        }

        if (objectNameEndIndex == S3_PROTOCOL_PREFIX.length()) {
            throw new IllegalArgumentException("The location :'" + location + "' does not contain a valid bucket name");
        }

        return location.substring(++objectNameEndIndex, location.length());
    }

    static String getLocationForBucketAndObject(String bucketName, String objectName) {
        Assert.notNull(bucketName, "Bucket name must not be null");
        Assert.notNull(objectName, "ObjectName name must not be null");
        StringBuilder location = new StringBuilder(S3_PROTOCOL_PREFIX.length() + bucketName.length() +
                PATH_DELIMITER.length() + objectName.length());
        location.append(S3_PROTOCOL_PREFIX);
        location.append(bucketName);
        location.append(PATH_DELIMITER);
        location.append(objectName);
        return location.toString();
    }

    static String getLocationForBucketAndObjectAndVersionId(String bucketName, String objectName, String versionId) {
        String location = getLocationForBucketAndObject(bucketName, objectName);
        return new StringBuffer(location).append(VERSION_DELIMITER).append(versionId).toString();
    }

    static String stripProtocol(String location) {
        Assert.notNull(location, "Location must not be null");
        if (!isSimpleStorageResource(location)) {
            throw new IllegalArgumentException("The location :'" + location + "' is not a valid S3 location");
        }
        return location.substring(S3_PROTOCOL_PREFIX.length());
    }
}