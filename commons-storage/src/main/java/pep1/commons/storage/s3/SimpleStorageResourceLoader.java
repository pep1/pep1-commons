/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.storage.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.util.ClassUtils;

/**
 * @author Agim Emruli
 * @author Alain Sahli
 * @since 1.0
 */
public class SimpleStorageResourceLoader implements ResourceLoader, InitializingBean {

    private final AmazonS3 amazonS3;
    private final ResourceLoader delegate;

    /**
     * <b>IMPORTANT:</b> If a task executor is set with an unbounded queue there will be a huge memory consumption. The
     * reason is that each multipart of 5MB will be put in the queue to be uploaded. Therefore a bounded queue is recommended.
     */
    private TaskExecutor taskExecutor;

    public SimpleStorageResourceLoader(AmazonS3 amazonS3, ResourceLoader delegate) {
        this.amazonS3 = amazonS3;
        this.delegate = delegate;
    }

    public SimpleStorageResourceLoader(AmazonS3 amazonS3, ClassLoader classLoader) {
        this.amazonS3 = amazonS3;
        this.delegate = new DefaultResourceLoader(classLoader);
    }

    public SimpleStorageResourceLoader(AmazonS3 amazonS3) {
        this(amazonS3, ClassUtils.getDefaultClassLoader());
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (this.taskExecutor == null) {
            this.taskExecutor = new SyncTaskExecutor();
        }
    }

    @Override
    public Resource getResource(String location) {
        if (SimpleStorageNameUtils.isSimpleStorageResource(location)) {
            return new SimpleStorageResource(this.amazonS3, SimpleStorageNameUtils.getBucketNameFromLocation(location),
                    SimpleStorageNameUtils.getObjectNameFromLocation(location), this.taskExecutor,
                    SimpleStorageNameUtils.getVersionIdFromLocation(location));
        }

        return this.delegate.getResource(location);
    }

    @Override
    public ClassLoader getClassLoader() {
        return this.delegate.getClassLoader();
    }
}