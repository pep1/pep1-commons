/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.rest.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import pep1.commons.rest.error.DefaultErrorController;
import pep1.commons.rest.error.RestHandlerExceptionAdvice;

@Configuration
@Import({
        MessageSourceAutoConfiguration.class
})
@Slf4j
public class RestConfig {


    private final MessageSource messageSource;

    private final ErrorAttributes errorAttributes;

    @Autowired
    public RestConfig(
            MessageSource messageSource,
            ErrorAttributes errorAttributes
    ) {
        this.messageSource = messageSource;
        this.errorAttributes = errorAttributes;
    }

    /**
     * For nice exception handling.
     *
     * @return new rest exception advice
     */
    @Bean
    public RestHandlerExceptionAdvice restHandlerExceptionAdvice() {
        return new RestHandlerExceptionAdvice(messageSource);
    }

    /**
     * For nice servlet exception handling.
     *
     * @return the error controller.
     */
    @Bean
    @ConditionalOnMissingBean(ErrorController.class)
    public DefaultErrorController defaultErrorController() {
        return new DefaultErrorController(errorAttributes);
    }

    /**
     * Custom validator to use all declared message sources in application.yml: spring.messages.basename
     *
     * @return the validator.
     */
    @Bean
    @Primary
    @Qualifier("mvcValidator")
    public Validator validator() {
        final LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setValidationMessageSource(messageSource);
        return validatorFactoryBean;
    }

}
