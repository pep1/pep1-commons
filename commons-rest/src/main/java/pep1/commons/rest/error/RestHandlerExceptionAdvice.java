/*
 * MIT License
 *
 * Copyright (c) 2018 Leonard Osang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pep1.commons.rest.error;

import com.fasterxml.jackson.core.JsonParseException;
import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pep1.commons.domain.message.ErrorMessage;
import pep1.commons.domain.message.ValidationErrorMessage;
import pep1.commons.error.ErrorCodes;
import pep1.commons.error.ParametrizedException;

@ControllerAdvice
@Slf4j
public class RestHandlerExceptionAdvice {

    private final MessageSource messageSource;

    @Autowired
    public RestHandlerExceptionAdvice(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(ParametrizedException.class)
    public ResponseEntity<ErrorMessage> illegalParameter(final ParametrizedException e) {
        log.error("Illegal parameter: " + e.getLocalizedMessage(), e);

        return new ResponseEntity<>(e.getErrorMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorMessage> illegalArgument(final IllegalArgumentException e) {
        log.error("Illegal argument: " + e.getLocalizedMessage(), e);

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.INVALID_INPUT,
                e.getLocalizedMessage()
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorMessage> validationFailed(final MethodArgumentNotValidException e) {
        log.error("Validation failed: " + e.getLocalizedMessage());

        ValidationErrorMessage message = new ValidationErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.VALIDATION_FAILED,
                "Validation failed"
        );

        e.getBindingResult().getFieldErrors().forEach(
                fieldError -> message.addError(
                        CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldError.getField()),
                        fieldError.getRejectedValue(),
                        fieldError.getDefaultMessage()
                )
        );

        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({
            JsonParseException.class
    })
    public ResponseEntity<ErrorMessage> jsonError(final JsonParseException e) {
        log.error("Parse Exception: " + e.getLocalizedMessage());

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.INVALID_INPUT,
                e.getLocalizedMessage()
        ), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({
            HttpMessageNotReadableException.class
    })
    public ResponseEntity<ErrorMessage> notReadableError(final NestedRuntimeException e) {
        log.error("Parse Exception: ", e);

        if (e.getRootCause() instanceof JsonParseException) {
            return jsonError((JsonParseException) e.getRootCause());
        }

        return new ResponseEntity<>(new ErrorMessage(
                HttpStatus.BAD_REQUEST,
                ErrorCodes.INVALID_INPUT,
                e.getLocalizedMessage()
        ), HttpStatus.BAD_REQUEST);
    }

}
